package me.juangoncalves.dimplespoc

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dimplesView = findViewById<DimplesView>(R.id.imageView)

        val options = BitmapFactory.Options()
        options.inTargetDensity = DisplayMetrics.DENSITY_DEFAULT
        val bmp = BitmapFactory.decodeResource(applicationContext.resources, R.drawable.carbg, options)
        dimplesView.maxCoordinate = Pair(bmp.width.toFloat(), bmp.height.toFloat())
        dimplesView.dimples = listOf(
            Pair(72f,355f),
            Pair(153f, 55f),
            Pair(162f, 67f),
            Pair(126f, 54f)
        )

        // fix #1
    }
}
