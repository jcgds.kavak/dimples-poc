package me.juangoncalves.dimplespoc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.widget.ImageView

class DimplesView : ImageView {
    private val TAG = javaClass.simpleName
    var debugMode: Boolean = false
    var dimples: List<Pair<Float, Float>> = listOf()
    var maxCoordinate: Pair<Float, Float> = Pair(width.toFloat(), height.toFloat())

    private val redPaint: Paint = Paint().apply { setARGB(255, 52, 129, 247) }
    private val debugPaint: Paint = Paint().apply { setARGB(255, 62, 52, 74) }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val radius: Float = Math.min(width, height).toFloat() / 60f
        drawDebugGuidelines(canvas, radius)

        dimples.forEach { coord ->
            transform(coord)
                .also {
                    canvas?.drawCircle(it.first, it.second, radius, redPaint)
                }
        }
    }

    /*
    * Dibuja un circulo en todas las esquinas y el centro de la vista.
    * Utilizado para verificar las dimensiones de la vista y la imagen que contiene.
    * */
    private fun drawDebugGuidelines(canvas: Canvas?, radius: Float) {
        if (!debugMode)
            return

        canvas?.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius, debugPaint)
        canvas?.drawCircle(0f, 0f, radius, debugPaint)
        canvas?.drawCircle(0f, height.toFloat(), radius, debugPaint)
        canvas?.drawCircle(width.toFloat(), height.toFloat(), radius, debugPaint)
        canvas?.drawCircle(width.toFloat(), 0f, radius, debugPaint)
    }

    /*
    * Encargado de ajustar las coordenadas provenientes del servidor que esperan que la imagen se muestre en una
    * resolucion de 320 x 490 px, calculando la proporcion con el height y width de esta vista para ubicar los
    * puntos correctamente.
    * */
    private fun transform(coord: Pair<Float, Float>): Pair<Float, Float> {
        val res = Pair( (width/maxCoordinate.first) * coord.first, (height/maxCoordinate.second) * coord.second)
        Log.d(TAG, "(${coord.first}, ${coord.second}) -> (${res.first}, ${res.second})")
        return res
    }
}